Personal Professional Milestones
================================

Overview
--------
My name is Derek Nutile and over the years I have placed my greasy hands on many types of technology.  This is a running log of large projects, minor projects, tasks that pushed me beyond my comfort zone, or forced me to use new technology, or simply items I wanted to note for later reference.  The list is in chronological order -- newest on top.  I'm sure I forgot a ton.

Find me at my personal website at [http://dereknutile.com/](http://dereknutile.com/).


Milestones & Projects List
--------------------------
* [Wrangler](#wrangler)
* [Dashboard](#dashboard)
* [Changelog](#changelog)
* [Passtore](#passtore)
* [STOPS Data Collection](#stops-data-collection)
* [CAD Repository](#cad-repository)
* [AS400 to SQL Migration Tool](#as400-to-sql-migration-tool)
* [AutoHotkey Scripts](#autohotkey-scripts)
* [Amazon Rekognition](#amazon-rekognition)
* [MollyWars](#mollywars)
* [Stashtokens](#stashtokens)
* [Filtersheet.com](#filtersheetcom)
* [ColdFusion Active Directory Authentication](#coldFusion-active-directory-authentication)
* [Parenting Together Website](#parenting-together-website)
* [Coldfusion Crash Resolution](#coldfusion-crash-resolution)
* [IT Toolbox](#it-toolbox)
* [Daily 135 Task Application](#daily-135-task-application)
* [Git Commit Guide](#git-commit-guide)
* [Drupal Vagrant Stack](#drupal-vagrant-stack)
* [Wordpress Vagrant Stack](#wordpress-vagrant-stack)
* [ClearBallot XML Parser](#clearballot-xml-parser)
* [Filtersheet](#filtersheet)
* [Netflix Cheatcodes](#netflix-cheatcodes)
* [Communications Contact Directory](#communications-contact-directory)
* [Web Development Best Practices](#web-development-best-practices)
* [Washington County Special Projects Site](#washington-county-special-projects-site)
* [FontAwesome CheatSheet](#fontawesome-cheatsheet)
* [Artbeats Express](#artbeats-express)
* [HostAnalytics](#hostanalytics)
* [JumbleBeard](#jumblebeard)
* [FADBUS FontAwesome Database](#fadbus)
* [MockMach Mockup Generation](#mockmach)
* [DerioLabs](#deriolabs)
* [PixelNewt](#pixelnewt)
* [Mastercraft ExactTarget Integration](#mastercraft-exactTarget-integration)
* [Converse Backstage](#converse-backstage)
* [AWS S3 Multiple Downloader](#aws-s3-zipper)
* [Converse Unleashed Awards](#converse-ua)
* [Converse Brandcheck](#converse-brandcheck)
* [Mastercraft Event Module](#mc-event-module)
* [Event App v2](#event-app-v2)
* [Flat Store UI](#flat-store-ui)
* [Board Member Vote App](#board-member-vote-app)
* [Top Selling Items Module](#top-selling-items)
* [Contracts & RFP Module](#contracts-rfp-module)
* [PCI Compliance](#pci-compliance)
* [Authorize.net Recapture Module](#auth-net-recapture)
* [Google Custom Search Engine](#google-cse)
* [Consortium Store CMS](#store-cms)
* [OETC Events App](#events-app)
* [CNET Data Integration](#cnet-integration)
* [Store Email Queue System](#email-queue-system)
* [Store Membership Quote Module](#member-quotes)
* [Store UI Upgrade](#store-ui-upgrade)
* [Store Parent Account Conversion](#store-parent-account-conversion)
* [Store Quoting System](#store-quote-system)
* [ITSC Raffle App](#itsc-raffle-app)
* [RDP Server Cloud Migration](#rdp-migration)
* [Greatplains Cloud Migration](#gp-migration)
* [Web Property Cloud Migration](#web-migration)
* [Store Downloads](#store-downloads)
* [Store Groups Module](#groups-module)
* [OETC CDN](#store-cdn)
* [Upgrade Internal RDP Server](#rdp-upgrade)
* [Virtualize Legacy Servers](#legacy-servers)
* [VM Server Upgrades](#vm-upgrades)
* [Google Mail Migration](#google-migration)
* [Convert Techhead to Magento](#techhead-magento)
* [Convert Store to CodeIgniter](#oetc-store-codeigniter)
* [OETC Website Move](#oetc-website-move)
* [Website Infrastructure Upgrade](#website-infrasructure-upgrade)
* [Accelerate Oregon](#accelerate-oregon)
* [Teach OETC Drupal Site](#teach-oetc)
* [ITSC Website](#itsc-website)

Dashboard
----------------------------

#### Wrangler
##### 2019 June

Laravel and VueJs based task manager.

In progress.

In progress.

#### Laravel and VueJs based dashboard
##### 2019 January

Laravel and VueJs based dashboard inspired by [Spatie Dashboard](https://github.com/spatie/dashboard.spatie.be).

In progress.

In progress.

[Repository](https://gitlab.com/dereknutile/dashboard).

``` Laravel, vuejs ```

Changelog
----------------------------

#### Laravel based [Semver versioning](https://semver.org/) change logger.
##### 2018 September

Inspired by the very simple [UIKit Changelog](https://getuikit.com/changelog), **Changelog** is an application to plan, track, and display changes.

In progress.

[Repository](https://gitlab.com/dereknutile/changelog).

``` Laravel, change, logging ```

Passtore
----------------------------

#### Laravel based version of a password keeper
##### 2018 July

In progress.

[Repository](https://gitlab.com/dereknutile/passtore).

``` Laravel, passwords, security, secrets ```

STOPS Data Collection
----------------------------

#### Created a STOPS data collection utility
##### 2018 June

Created several applications needed for the collecting and submission of STOPS data mandated by House Bill 2355.

[Repository](https://gitlab.com/ccso-developers/ccso-stops-collection)

``` ASP, PHP, PowerShell, SQL ```

CAD Repository
----------------------------

#### Developed CAD Repository
##### 2018 March

Simply put, I created a repository for the CAD system that maps the different configurations into different branches of the git repo.

[Repository](https://gitlab.com/ccso-developers/ccso-cad-configuration-files)

``` Git ```

Milestones & Projects Detail
----------------------------

#### AS400 to SQL Migration Tool
##### 2017 December

Created a simple command-line scripting process using PHP to migrate tables from IBM/DB2 AS400 to Microsoft SQL Server.  This is a line by line process that copies DB2 data to Microsoft SQL.  The script will read a block of lines (default is 10000) from the source (DB2) and process that block to the destination (SQL).  Note, the database itself is not created on the destination SQL server.

[Repository](https://gitlab.com/ccso-developers/db2-to-sql)

``` PHP, IBM, DB2, AS400, SQL ```

#### AutoHotkey Scripts
##### 2017 October - 2018 July

Developed a set of scripts to extract data from a closed source Java application.  I used AutoHotkey to script the step by step process of building and generating over 100,000 PDF documents.

[Repository](https://gitlab.com/ccso-developers/ccso-auto-hot-key-scripts)

``` Autohotkey, cmd, scripts ```

#### Amazon Rekognition
##### 2017 September

Application that allows users to upload an image to AWS S3 and have it compared against a large indexed dataset of images.  The tool is written in PHP/Laravel and connects to Amazon S3 and [Rekognition](https://aws.amazon.com/rekognition/) via the AWS SDK. This is an internal application for the Clackamas County Sheriff's Office.

``` Amazon, S3, Rekognition, API, PHP, Laravel ```

#### Stashtokens
##### 2017 July

Laravel application to store and generate secrets (previously named BrambleBeard.  The application allows a non-authenticated sharing service as well as a signed-in storage for secrets.  Source:[https://gitlab.com/dereknutile/stashtokens](https://gitlab.com/dereknutile/stashtokens).

``` Laravel, passwords, security, secrets ```

#### Filtersheet.com
##### 2017 June

Launched filtersheet.com, a static Jekyll application that can consume data files in JSON or YAML and display the content in easily filterable output.  Source code:[https://github.com/filtersheet/filtersheet.github.io](https://github.com/filtersheet/filtersheet.github.io), website: [http://filtersheet.com/](http://filtersheet.com/).

``` Jekyll, passwords, security, secrets ```

#### BrambleBeard Mockup
##### 2017 April

Designed mockup for future application to securely store secrets.  See the mockup here: [https://github.com/BrambleBeard/bramblebeard.github.io](https://github.com/BrambleBeard/bramblebeard.github.io).

``` Jekyll, passwords, security, secrets ```

#### ColdFusion Active Directory Authentication
##### 2017 March

Converted existing authentication scripts from Novell IDS to Active Directory.

``` development, code, ColdFusion, CommonSpot, CMS ```

#### Parenting Together Website
##### 2017 February

Performed DevOps for the Washington County Health & Human Services department to plan and launch the [http://parentingtogetherwc.org](http://parentingtogetherwc.org) website on Wordpress.  Performed domain purchase, setup hosting, backups, and created a test and production environment.

``` DevOps, wordpress, hosting ```

#### Coldfusion Crash Resolution
##### 2017 January

For years, the infrastructure supporting the Washington County web properties was unstable.  Through research, I discovered the issue to be Tomcat and ISAPI related.  ISAPI was getting overwhelmed because the version of the ISAPI dll was not working with the version of ColdFusion.  Performance was degraded due to a misconfiguration of Tomcat threads and timeouts.

``` Coldfusion, tomcat, IIS, ISAPI ```

#### IT Toolbox
##### 2016 December

Attempting to address missing internal IT utilities, I built a web application which can collect work orders, documentation, software and asset information, and key and security logging all tied to a unified directory.  The toolbox has a clean dashboard with several reports.  See the mockup here: [https://github.com/dereknutile/toolbox-mockup](https://github.com/dereknutile/toolbox-mockup).

``` Laravel, Bootstrap, PHP, utility, documentation, software, assets, keys, directory ```

#### Daily 123 Task Application
##### 2016 November

Inspired by [an article I read](https://www.themuse.com/advice/a-better-todo-list-the-135-rule) on simplifying your personal task list, I built the Daily135 web application.  See the mockup here [https://github.com/dereknutile/daily123-mockup](https://github.com/dereknutile/daily123-mockup).

``` Bulma, Laravel, PHP, VueJS ```



#### Git Commit Guide
##### 2016 April

Created a living document created to help guide the delicate, often tedious process, of writing commit messages with the goal of establishing a convention for Git commits to ultimately produce legible, searchable, quality Git logs..  Find it here: [https://github.com/dereknutile/git-commit-guide](https://github.com/dereknutile/git-commit-guide).

``` Jekyll, documentation, standards, git, dvcs ```

#### Drupal Vagrant Stack
##### 2016 March

Created a repository to use as a template for Drupal projects built on Vagrant: [https://github.com/dereknutile/vlemp-drupal](https://github.com/dereknutile/vlemp-drupal).  This repo can be used as a base for any version of Drupal development and automates the installation of several aspects of the framework.

``` PHP, Vagrant, automation, Drupal ```

#### Wordpress Vagrant Stack
##### 2016 March

Created a repository to use as a template for Wordpress projects built on Vagrant: [https://github.com/dereknutile/vlemp-wordpress](https://github.com/dereknutile/vlemp-wordpress).  This repo can be used as a base for any version of Wordpress development and automates the installation of several aspects of the framework.

``` PHP, Vagrant, automation, Wordpress ```

#### ClearBallot XML Parser
##### 2016 January

Created a simple PHP application to parse [ClearBallot](http://www.clearballot.com/) XML for the Washington County Elections office.  Details of this project include the ability for team users to customize the output slightly including options like next election polling date and time and alerts.  The goal of this small project was to generate HTML output so election results could be displayed and filtered by the the citizens of Washington County.

``` PHP, XML, ClearBallot, Washington County, WASHCO ```

#### Filtersheet
##### 2015 November

[http://filtersheet.github.io/](http://filtersheet.github.io/) exists to ease the process of finding and filtering web candy.

```  Jekyll, Filter ```

#### Communications Contact Directory
##### 2015 November

Designed and implemented a directory system for an internal department to keep track of internal and external recipients.

``` PHP, Laravel, MSSQL, Washington County ```

#### Netflix Cheatcodes
##### 2016 January

I built the [Netflix Cheatcodes](http://netflix-cheatcodes.github.io/) using a similar filter as the FontAwesome Database project.  I first saw the Netflix cheat codes on Reddit and tried to rapidly post a better solution.

``` Bootstrap, Jekyll, Filter, Netflix ```

#### Communications Contact Directory
##### 2015 November

Designed and built a Laravel application for the Washington County LUT department.  This application is mainly a contact list that the admin team can manage for external communication

``` PHP, Laravel, CMS, Washington County, WASHCO ```

#### Web Development Best Practices
##### 2015 September

This is an ongoing project to define a collection of web development best practices.  Find it here: [http://wdbp.github.io/](http://wdbp.github.io/).

``` Jekyll, documentation, standards ```

#### Washington County Special Projects Site
##### 2015 May

[Washington County Special Projects Site](http://washcospecialproj.com/) was built for the county of Washington to share information related to the proposed commissioner district changes.  The application is meant to be informational and offer simple feedback and analytics.

``` Washington County, Laravel, GoDaddy ```

#### FontAwesome CheatSheet
##### 2015 January

The [FontAwesome Database](http://fadbus.github.io/) site is a tool I created because finding [FontAwesome](http://fortawesome.github.io/Font-Awesome/) fonts is a pain.  This was mostly a redesign but it was from the ground up.

``` FontAwesome, Glyphs, Icons, Bootstrap, Jekyll, Filter ```

#### Artbeats Express
##### 2014 October

[Artbeats Express](http://artbeatsexpress.com/) is a large technical project I worked on for GRAYBOX as a full stack Ruby on Rails developer.  Artbeatsexpress is an application for buying and selling professional level video and other media.

``` Graybox, Ruby, Rails, Postgres, AWS ```

#### HostAnalytics
##### 2014 October

[HostAnalytics](http://hostanalytics.com/) was a project I worked on at GRAYBOX.  My role started as the front end developer, but extended to more of a full-stack assistant to the lead developer (Santiago Barros).

``` Graybox, PHP, Javascript, ExpressionEngine, Front end, Back end ```

#### JumbleBeard
##### 2014 October

[JumbleBeard](http://jumblebeard.com/) is a web tool used to share developer information that should be kept secret outside of the team -- like passwords, AWS credentials, project configuration settings, etc.

``` Personal, Security, Disposable ```

#### FADBUS
##### 2014 September

[FADBUS](http://fadbus.github.io/) is a simple lookup tool for [FontAwesome](http://fontawesome.io/) icons.  FADBUS is in the second iteration and a joint effort with [Dean Dowd](https://github.com/deandowd).

``` Personal, Jekyll, Bootstrap, FontAwesome ```

#### [MockMach](id:anchor-mockmach)
##### 2014 September

Designed and developed the [MockMach](http://mockmach.com/) website.  MockMach is my personal tool to rapidly turn wireframes, PSD's, and ideas into live, functioning, beautiful websites.

``` Personal, Jekyll, Bootstrap ```

#### [Deriolabs](id:anchor-deriolabs)
##### 2014 August

Designed and developed the [Deriolabs](http://deriolabs.com/) website.

``` Personal, Jekyll, Bootstrap ```

#### [Pixelnewt](id:anchor-pixelnewt)
##### 2014 August

Designed and developed the [PixelNewt](http://pixelnewt.com/) website.

``` Personal, Jekyll, Bootstrap ```

#### [Mastercraft ExactTarget Integration](id:anchor-mastercraft-exactTarget-integration)
##### 2014 July

Integrate site to ExactTarget.

``` TheGood, PHP, Mastercraft, ExactTarget ```

I worked with ExactTarget to get the Mastercraft website connected using their [FuelSDK](https://github.com/ExactTarget/FuelSDK-PHP) API.  The API had issues, to the point where I had contributed to the codebase.  In the end, I ended up using a more legacy SOAP based solution.

---

#### [Converse Backstage](id:anchor-converse-backstage)
##### 2014 June
###### Site: [https://backstage.converse.com](https://backstage.converse.com)

This is the final release of the Backstage project, combined with the Unleashed awards project and all the other pieces that complete the app.  More to come.

``` TheGood, Django, Converse, Backstage, AWS, EC2, RDS ```

---


#### [AWS S3 Multiple Downloader](id:anchor-aws-s3-zipper)
##### 2014 April
###### Site:

Amazon S3 does not have a utility to collect files and compress them before delivering.  [Larry Beal](https://github.com/bealtech) and I had the challenge of allowing Converse Backstage users the ability to select as many files as they want, and as large as they want (up to 4GB), and returning one simple zip file.

``` TheGood, Django, ZZTOP, Converse, Backstage, AWS, EC2, RDS ```

---


#### [Converse Unleashed Awards](id:anchor-converse-ua)
##### 2014 April
###### Site: [https://backstage.converse.com](https://backstage.converse.com)

The core code for this project was done by [Larry Beal](https://github.com/bealtech). [Laura Fischer](https://github.com/laurafischer) and I scoped the project out, and Laura worked much of the front end.  Aside from scoping, I handled much of the devops and several fixes and minor add-ons.

``` TheGood, Django, Converse, Backstage, AWS, EC2, RDS ```

---


#### [Converse Brandcheck](id:anchor-converse-brandcheck)
##### 2013 December
###### Site: [https://backstage.converse.com](https://backstage.converse.com)

The front end development for this project was built by [Laura Fischer](https://github.com/laurafischer).  This is a Django project hosted on Amazon EC2 using Postgres RDS.  The first phase of the Converse portal project combines several apps including asset uploading & management, multiple dynamic CMS type areas, notifications, expirations, linkage with Amazon AWS services, and more.

The first phase is simply to stand up the application skeleton for QA testing.

``` TheGood, Django, Converse, Brandcheck, AWS, EC2, RDS ```

---

#### [Mastercraft Event Module](id:anchor-mc-event-module)
##### 2013 November
###### Site: [http://mastercraft.com/events/calendar](http://mastercraft.com/events/calendar)

This was a very simple CodeIgniter module I created to handle events through the existing CMS.  This was mostly a "learn the codebase" exercise but I was able to build and ship the module ahead of my deadline.

``` TheGood, PHP, CodeIgniter, MySQL ```

---

#### [Event App v2](id:anchor-event-app-v2)
##### 2013 July
###### Site: [http://events.oetc.org/](http://events.oetc.org/)

I re-engineered the existing event app using a flat design mocked up by the OETC marketing designer.  The project goals were to apply a newer UI and create a new routing system so the url could flow within the context of the event.  For example, the old site url for the ITSC schedule would look like this:


The new route included logic to pass any event page through an event router which resulted in a slimmer, dynamic url: [http://events.oetc.org/event/itsc/schedule](http://events.oetc.org/event/itsc/schedule)

Many enhancements to the event app were included like:
* a more robust administrative panel
* CMS inputs for custom and dynamic page creation
* the raffle application was ported into the project

``` OETC, PHP, CodeIgniter, MySQL, Twitter Bootstrap, OmniGraffle ```

---

#### [Flat Store UI](id:anchor-flat-store-ui)
##### 2013 July

Working with the OETC marketing designer, we setup the framework for the new store ui using Twitter Bootstrap on OmniGraffle mockups.  The design was applied to both the front end and administrative back end of the application.

Within the redesign, other interface updates were implemented like a simpler user management area on the front end and a very effective “Jump Search” for the operations team to rapidly find just about anything in the OWA (OETC Web Admin) area.

``` OETC, PHP, CodeIgniter, MySQL, Twitter Bootstrap, OmniGraffle ```

---

#### [Top Selling Items Module](id:anchor-top-selling-items)
##### 2013 July
To generate a better landing page for the store, we decided to create a few MySQL tables generated from GreatPlains that offered a subset of the top selling items for the store.

There were a few hurdles to consider like the odd spikes our catalog could see from massive purchases (like a large school district purchasing an enormous volume of licenses).  Also, the visibility of the catalog had to be considered since some items are hidden from non-authenticated users.

The end result is a few automatically and randomly generated blocks of top selling items on the landing page as well as the top of some brand pages.  Additionally, item blocks are sometimes generated by category (like projectors for example).

``` OETC, PHP, CodeIgniter, MySQL, Microsoft SQL, Microsoft Dynamics GP ```

---

#### [Board Member Vote App](id:anchor-board-member-vote-app)
##### 2013 June
###### Site: [http://vote.oetc.org/](http://vote.oetc.org/)

I was tasked to simply create an application that allowed for authenticated users to select from a ballot of candidates for a few board membership roles.  The ballots have to only be available to the voters for a window of time (a few weeks) and must close once the voter chooses their candidates.

This was a small application, so I took the opportunity to use a different framework this time: Laravel.


``` OETC, PHP, Laravel, MySQL ```

---

#### [Contracts & RFP Module](id:anchor-contracts-rfp-module)
##### 2013 May
###### URL's: [https://store.oetc.org/contracts](https://store.oetc.org/contracts), [https://store.oetc.org/rfps](https://store.oetc.org/rfps),

The project goal was to offer a single place for members to review the RFP process and contracts for the OETC store catalog.

Rfps and contracts are created in the store administration area linking to documents stored on Google Drive.  RFP’s are associated with brands so there is linkage from rfps and contracts to brands as well as a convenient link to the current contract on each brand and item page.

Finally, the module must respect the visibility of brands and items, as well as offer a start date and expiration for each RFP and contract.

``` OETC, PHP, CodeIgniter, MySQL ```

---

#### [PCI Compliance](id:anchor-pci-compliance)
##### 2013 May
The executive director put in place policies and expectations to handle the administrative requirements of PCI compliance.

My responsibility was to run tests and assess warnings and failures based on the results.  Most of the assessed issues were addressed by either removing/closing services or ports and patching systems and applications.

Final result: We passed Trustwave tests for PCI compliance.

``` OETC, PCI, Trustwave ```

---

#### [Authorize.net Recapture Module](id:anchor-auth-net-recapture)
##### 2013 May

The OETC store’s payment gateway is Authorize.net.  This has been used for some time to authorize and capture funds on purchase.

The issue comes when orders are only partially fulfilled and the authorization expires (typically after 30 days).  This occurs often at OETC because the vendor may need time to process and ship large items like furniture.

The module addresses this by performing a re-authorization and a capture upon any new invoices after 30 days.

``` OETC, PHP, CodeIgniter, MySQL, Authorize.net API ```

---

#### [Google Custom Search Engine](id:anchor-google-cse)
##### 2013 March
###### Site: [http://store.oetc.org/gse](http://store.oetc.org/gse)

The store has been using a local search indexer (Zend Lucene) but the results are often poor.

The Google CSE project is to simply setup and configure the Google CSE in Google admin, create the new routes and sitemaps, and test.  The results were pretty good, but there seemed to be latency in the site crawls and visually the results did not match the store ui.

The store still offers the Google CSE, but by default the search box runs the Zend Lucene index.  The Google search url is obfuscated but Google still indexes the site.
URL

``` OETC, PHP, CodeIgniter, MySQL, Zend Framework, Lucene, Google Custom Search Engine ```

---

#### [Consortium Store CMS](id:anchor-store-cms)
##### 2013 April

Added cms ability to portions of the store including blocks and the brand pages.
Technology

``` OETC, PHP, CodeIgniter, MySQL ```

---

#### [OETC Events App](id:anchor-events-app)
##### 2012 August
###### Site: [http://events.oetc.org/](http://events.oetc.org/)
Built and delivered for integratED SF

``` OETC, PHP, CodeIgniter, MySQL ```

---

#### [CNET Data Integration](id:anchor-cnet-integration)
##### 2012 July

The OETC store catalog is large and unmanageable.  The goal of this project was to connect the live store catalog with the images and descriptions managed by the Cnet Datasource.

The implementation was to create a middle-tier database that, on a schedule, updated the current list of OETC Consortium store sku's and link them with data supplied by the Cnet Datasource.

``` OETC, PHP, CodeIgniter, MySQL, Java, Linux, scripting, Cnet Datasource, Cnet API ```

---

#### [Store Email Queue System](id:anchor-email-queue-system)
##### 2012 July

We found a limit to what Google would allow for outbound emails being sent at one time.  The problem would cause emails to stop being sent and then lost.

Because the membership renewal could send hundreds of emails at a time, I created an email queueing system that would process queued emails X per minute.


``` OETC, PHP, CodeIgniter, MySQL ```

---

#### [Store Membership Quote Module](id:anchor-member-quotes)
##### 2012 June

Created a module for the store to generate quotes for member drive.


``` OETC, PHP, CodeIgniter, MySQL ```

---

#### [Store UI Upgrade](id:store-ui-upgrade)
##### 2012 May

Converted the entire front and back end of the existing store to Twitter Bootstrap.


``` OETC, PHP, CodeIgniter, MySQL, Twitter Bootstrap 2 ```

---


#### [Store Parent Account Conversion](id:store-parent-account-conversion)
##### 2012 March

Converted all Microsoft Dynamics Greatplains 10 child accounts to member groups in the store.


``` OETC, PHP, CodeIgniter, MySQL, Microsoft SQL, Microsoft Dynamics GP 10 ```

---

#### [Store Quoting System](id:anchor-store-quote-system)
##### 2012 March

Implemented store quote system for customers and operations.

``` OETC, PHP, CodeIgniter, MySQL```

---

#### [ITSC Raffle App](id:anchor-itsc-raffle-app)
##### 2012 February
##### Site: [http://itscraffle.oetc.org/](http://itscraffle.oetc.org/)

Created a simple application to raffle off prizes at ITSC.  Very successful and well received.

``` OETC, PHP, CodeIgniter, MySQL ```

---


#### [RDP Server Cloud Migration](id:anchor-rdp-migration)
##### 2012 January

Migrated the terminal server to the cloud - also upgraded the server OS and applications.

``` OETC, Linux, VMware, Amazon AWS, Rackspace, Microsoft Dynamics GP 10 ```

---
anchor-gp-migration)
##### 2011 December

Migrated the Greatplains server to the Rackspace cloud.

``` OETC, Linux, VMware, Amazon AWS, Rackspace, Microsoft Dynamics GP 10 ```

---

#### [Web Property Cloud Migration](id:anchor-web-migration)
##### 2011 December

Moved web server and web database to the Rackspace cloud.

``` OETC, Linux, VMware, Amazon AWS, Rackspace ```

---

#### [Store Downloads](id:anchor-store-downloads)
##### 2011 December

Create a framework to associate media with downloadable files on the store.

``` OETC, PHP, CodeIgniter, MySQL ```

---

#### [Store Groups Module](id:anchor-groups-module)
##### 2011 December
Members (Beaverton) asked to be able to group their users for custom shipping as well as custom history.

``` OETC, PHP, CodeIgniter, MySQL ```

---

#### [OETC CDN](id:anchor-store-cdn)
##### 2011 November
Create a content delivery site for OETC.
URL
http://cdn.oetc.org/

``` OETC, PHP, CodeIgniter, MySQL ```

---

#### [Upgrade Internal RDP Server](id:anchor-rdp-upgrade)
##### 2010 October

Deprecated the Inside server and created a new VM for the new Inside application server.

``` OETC, VMWare, Linux ```

---

#### [Virtualize Legacy Servers](id:anchor-legacy-servers)
##### 2010 October

Virtualize GP8, eduhost, and Inside after the VM Server upgrades.

``` OETC, VMWare, Linux ```

---

#### [VM Server Upgrades](id:anchor-vm-upgrades)
##### 2010 July

Converted all VM's to VMWare 2.0. Reconfigured topology to secure "inside" servers.

``` OETC, VMWare, Linux ```

---

#### [Google Mail Migration](id:anchor-google-migration)
##### 2010 June

Route e-mail through Google Apps instead of OETC.

``` OETC, Linux, Postfix/sendmail, Google Apps, DNS ```

---

#### [Convert Techhead to Magento](id:anchor-techhead-magento)
##### 2010 June

Fully completed the techhead.org store migration using Magento
URL
http://techhead.org/

``` OETC, Linux, Apache, PHP, Magento ```

---

#### [Convert Store to CodeIgniter](id:anchor-oetc-store-codeigniter)
##### 2010 February
##### Site: [http://store.oetc.org/](http://store.oetc.org/)

Migrated and cleaned the existing Perl store codebase and applied a new CSS layout

``` OETC, Linux, Apache, PHP, CodeIgniter, Perl ```

---

#### [OETC Website Move](id:anchor-oetc-website-move)
##### 2010 February
##### Site: [http://oetc.org/](http://oetc.org/)

Successfully and quickly planned and implemented a Wordpress site move to replace aging oetc.org domain landing pages.

``` OETC, Linux, Apache, Wordpress ```

---

#### [Website Infrastructure Upgrade](id:anchor-website-infrasructure-upgrade)
##### 2010 January

I created new virtual servers for site hosting.  The upgrade included building new VMWare Server templates, migrating all OETC web properties.

``` OETC, VMWare, Linux ```

---

#### [Accelerate Oregon](id:anchor-accelerate-oregon)
##### 2009 November
##### Site: [http://ao.oetc.org/](http://ao.oetc.org/)

Built a Magento 1.7.2 store to temporarily promote a catalogue for a group of clients.

``` OETC, PHP, MySQL, Zend Framework, Magento (1.7.2) ```

---

#### [Teach OETC](id:teach-oetc)
##### 2009 November
##### Site: [http://teach.oetc.org/](http://teach.oetc.org/)

Helped build the OETC Teach site on the Drupal platform.

``` OETC, PHP, MySQL, Drupal ```

---

#### [ITSC Website](id:anchor-itsc-website)
##### 2009 November
##### Site: [http://itsc.oetc.org/](http://itsc.oetc.org/)

I was tasked to complete and test the ITSC (Instructional Technologies Strategy Conference) web site and user registration site.  Registration started in November and the site was built on time.

``` OETC, PHP, MySQL ```
